package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
)

var logger = logrus.New()

func main() {
	logger.Out = os.Stdout
	logger.SetLevel(logrus.DebugLevel)
	if formatter, ok := logger.Formatter.(*logrus.TextFormatter); ok {
		formatter.FullTimestamp = true
	}
	// You could set this to any `io.Writer` such as a file
	// file, err := os.OpenFile("logrus.logger", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	// if err == nil {
	//  logger.Out = file
	// } else {
	//  logger.Info("Failed to logger to file, using default stderr")
	// }

	logger.WithFields(logrus.Fields{
		"animal": "walrus",
		"size":   10,
	}).Info("A group of walrus emerges from the ocean")
	logger.Warnf("A warning logger")
	logger.Warn("A warning logger")
	logger.Errorf("A errors logger")
	//log.Printf("abc")
	logger.Debugf("a debug logger")
	func() {
		defer func() {
			entry := recover()
			if e, ok := entry.(*logrus.Entry); ok {
				fmt.Println(e.String())
			}
		}()
		logger.Panicf("A panic logger")
	}()
	func() {
		logger.Fatalf("a fatal logger")
		defer func() {
			err := recover()
			fmt.Println(err)
		}()
	}()
}
