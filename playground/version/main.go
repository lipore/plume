package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println(runtime.Compiler)
	fmt.Println(runtime.Version())
	fmt.Printf("%s/%s", runtime.GOOS, runtime.GOARCH)
}
