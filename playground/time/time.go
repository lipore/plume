package main

import (
	"fmt"
	"gitee.com/lipore/plume/logger"
	"time"
)

func main() {
	fmt.Println(time.Now())
	fmt.Println(time.Now().Zone())
	fmt.Println(time.Now().Format("2006/01/02 15:04:05"))

	t, err := time.Parse("2006/01/02", "2022/08/22")
	if err != nil {
		logger.Error(err.Error())
	}

	fmt.Println(t)
	fmt.Println(t.Format(time.RFC3339))
	fmt.Println(t.Local())
	fmt.Println(t.Format(time.RFC3339))

	fmt.Println(time.Parse(time.RFC3339, "2019-12-05T12:04:09Z"))
}
