package main

import (
	"context"
	"fmt"
	"time"
)

func layer1(ctx context.Context) {
	ctx2, cancel := context.WithCancel(ctx)
	go layer2(ctx2)
	timer := time.NewTimer(1 * time.Second)
	select {
	case <-timer.C:
		cancel()
	}
	fmt.Println("layer1 done")
}

func layer2(ctx context.Context) {
	go layer3(ctx)
	<-ctx.Done()
	fmt.Println("layer2 done")
}

func layer3(ctx context.Context) {
	_, cancel := context.WithCancel(ctx)
	cancel()
	<-ctx.Done()
	fmt.Println("layer3 done")
}

func main() {
	layer1(context.Background())
}
