package test

type M[T any] struct {
	s []T
}

func (m *M[any]) PrintSlice() {
	for _, v := range m.s {
		print(v)
	}
}
