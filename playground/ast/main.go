package main

import (
	"fmt"
	"go/parser"
	"go/token"
)

func PrintSlice[T any](s []T) {
	for _, v := range s {
		print(v)
	}
}

func main() {
	fset := token.NewFileSet()
	file, err := parser.ParseDir(fset, "test", nil, parser.ParseComments)
	if err != nil {
		panic(err)
	}
	for _, decl := range file.Decls {
		fmt.Printf(decl)
	}
}
