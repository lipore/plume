package main

import "fmt"

type A struct {
	a int
}

func (a *A) printp() {
	fmt.Printf("a.printp %p\n", a)
	a.a = 1
	fmt.Printf("a.printp %v\n", a)
}

func (a A) print() {
	fmt.Printf("a.print %p\n", &a)
	a.a = 2
	fmt.Printf("a.print %v\n", a)
}

func print(a A) {
	fmt.Printf("print %p\n", &a)
	a.a = 3
	fmt.Printf("print %v\n", a)
}

func printp(a *A) {
	fmt.Printf("printp %p\n", a)
	a.a = 4
	fmt.Printf("printp %v\n", a)
}

func main() {
	a := A{a: 0}
	fmt.Printf("%p\n", &a)
	a.printp()

	fmt.Printf("\n- %v\n", &a)
	a.print()

	fmt.Printf("\n- %v\n", &a)
	(&a).print()

	fmt.Printf("\n- %v\n", &a)
	print(a)

	fmt.Printf("\n- %v\n", &a)
	printp(&a)

	fmt.Printf("\n- %v\n", &a)

	as := make([]A, 4)
	for i, a := range as {
		a.a = i
	}
	fmt.Printf("= %v\n", as)
	for i, _ := range as {
		as[i].a = i
	}
	fmt.Printf("= %v\n", as)
}
