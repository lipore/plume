package main

import (
	"fmt"
	"strconv"
)

type withCode struct {
	code int
	msg  string
}

func (c *withCode) Format(state fmt.State, verb rune) {
	switch verb {
	case 's':
		state.Width()
		state.Write([]byte(c.msg))
	case 'd':
		state.Write([]byte(strconv.FormatInt(int64(c.code), 10)))
	default:
		state.Write([]byte("nil"))
	}
}
