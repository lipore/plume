package main

import (
	"errors"
	"fmt"
)

type Astruct struct {
	f1 int
	f2 float32
	f3 string
	f4 *Bstruct
}

type Bstruct struct {
	f4 bool
}

func (b *Bstruct) String() string {
	return fmt.Sprintf("%t", b.f4)
}

func main() {
	a := Astruct{
		f1: 1,
		f2: 1.2,
		f3: "1.2",
		f4: &Bstruct{f4: false},
	}
	fmt.Printf("%%v is: %v\n", a)
	fmt.Printf("%%+v is: %+v\n", a)
	fmt.Printf("%%-v is: %-v\n", a)
	fmt.Printf("%%#-v is: %#-v\n", a)
	fmt.Printf("%%#+v is: %#+v\n", a)
	fmt.Printf("%%T is: %T\n", a)

	err := errors.New("error1")
	fmt.Printf("error 1 : %#v\n", err)

	w := withCode{code: 123, msg: "string message"}
	fmt.Printf("%s\n", &w)
	fmt.Printf("%d\n", &w)
	fmt.Printf("%v\n", &w)
}
