package main

import (
	"gitee.com/lipore/plume/option"
	"os"

	"github.com/spf13/pflag"
)

var output = pflag.String("o", "", "The output file")

func main() {

	pflag.Parse()

	// We accept either one directory or a list of files. Which do we have?
	args := pflag.Args()
	if len(args) == 0 {
		// Default: process whole package in current directory.
		args = []string{"."}
	}

	os.Remove(*output)
	g := &option.Generator{
		FilePath:         args[0],
		OptionStructName: "Options",
	}

	codes := g.Generate()

	f, err := os.Create(*output)
	if err != nil {
		panic(err)
	}
	f.Write(codes)
	f.Close()
}
