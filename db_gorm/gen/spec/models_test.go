package spec

import (
	"testing"
)

type OperationTestCase struct {
	Operation    Operation
	ExpectedName string
}

func TestOperationName(t *testing.T) {
	testTable := []OperationTestCase{
		{
			Operation:    InsertOperation{},
			ExpectedName: "Insert",
		},
		{
			Operation:    FindOperation{},
			ExpectedName: "Find",
		},
		{
			Operation:    UpdateOperation{},
			ExpectedName: "Update",
		},
		{
			Operation:    DeleteOperation{},
			ExpectedName: "Delete",
		},
		{
			Operation:    CountOperation{},
			ExpectedName: "Count",
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.ExpectedName, func(t *testing.T) {
			if testCase.Operation.Name() != testCase.ExpectedName {
				t.Errorf("Expected = %+v\nReceived = %+v", testCase.ExpectedName, testCase.Operation.Name())
			}
		})
	}
}
