package spec

import (
	"testing"

	"gitee.com/lipore/plume/db_gorm/gen/code"
)

type ErrorTestCase struct {
	Name           string
	Error          error
	ExpectedString string
}

func TestError(t *testing.T) {
	testTable := []ErrorTestCase{
		{
			Name:           "UnknownOperationError",
			Error:          NewUnknownOperationError("Search"),
			ExpectedString: "unknown operation 'Search'",
		},
		{
			Name:           "StructFieldNotFoundError",
			Error:          NewStructFieldNotFoundError([]string{"Phone", "Number"}),
			ExpectedString: "struct field 'PhoneNumber' not found",
		},
		{
			Name:           "UnsupportedReturnError",
			Error:          NewUnsupportedReturnError(code.SimpleType("User"), 0),
			ExpectedString: "return type 'User' at index 0 is not supported",
		},
		{
			Name:           "OperationReturnCountUnmatchedError",
			Error:          NewOperationReturnCountUnmatchedError(2),
			ExpectedString: "operation requires return count of 2",
		},
		{
			Name:           "InvalidQueryError",
			Error:          NewInvalidQueryError([]string{"And"}),
			ExpectedString: "invalid query 'And'",
		},
		{
			Name: "IncompatibleComparatorError",
			Error: NewIncompatibleComparatorError(ComparatorTrue, code.StructField{
				Name: "Age",
				Type: code.SimpleType("int"),
			}),
			ExpectedString: "cannot use comparator EQUAL_TRUE with struct field 'Age' of type 'int'",
		},
		{
			Name:           "InvalidSortError",
			Error:          NewInvalidSortError([]string{"Order", "By"}),
			ExpectedString: "invalid sort 'OrderBy'",
		},
		{
			Name:           "ArgumentTypeNotMatchedError",
			Error:          NewArgumentTypeNotMatchedError("Age", code.SimpleType("int"), code.SimpleType("float64")),
			ExpectedString: "field 'Age' requires an argument of type 'int' (got 'float64')",
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.Name, func(t *testing.T) {
			if testCase.Error.Error() != testCase.ExpectedString {
				t.Errorf("Expected = %+v\nReceived = %+v", testCase.ExpectedString, testCase.Error.Error())
			}
		})
	}
}
