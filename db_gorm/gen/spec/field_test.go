package spec

import (
	"gitee.com/lipore/plume/db_gorm/gen/code"
	"testing"
)

func TestFieldReference_ReferencingCode(t *testing.T) {
	fieldReference := FieldReference{
		{Name: "package", Type: code.SimpleType("int")},
		{Name: "model", Type: code.SimpleType("int")},
		{Name: "field", Type: code.SimpleType("int")},
	}
	fieldReferenceCode := fieldReference.ReferencingCode()
	expectCode := "package.model.field"
	if expectCode != fieldReferenceCode {
		t.Errorf("ReferencingCode error")
	}
}
