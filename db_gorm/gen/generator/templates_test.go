package generator

import (
	"bytes"
	"fmt"
	"gitee.com/lipore/plume/testutils"
	"testing"
	"text/template"
)

func TestObjectMapperTemplate(t *testing.T) {
	t.Run("generate mapper", func(t *testing.T) {
		templ, err := template.New("objectMapper").Parse(objectMapper)
		if err != nil {
			t.Error(err)
		}

		templData := objectMapperData{
			EntityName:       "IncomeEntity",
			DomainObjectCode: "domain.Income",
			FieldMap: []objectMapperField{
				{DomainField: "Id", EntityField: "Id", Entity2DomainFunc: "int64", Domain2EntityFunc: "db_gorm.ID", NeedConvert: true},
				{DomainField: "Name", EntityField: "Name", Entity2DomainFunc: "string", Domain2EntityFunc: "string", NeedConvert: false},
			},
		}

		buffer := new(bytes.Buffer)

		if err := templ.Execute(buffer, templData); err != nil {
			t.Error(err)
		}

		expectString := `func (entity *IncomeEntity) toDomainObject() *domain.Income {
    return &domain.Income{
        Id: int64(entity.Id),
        Name: entity.Name,
    }
}

func (entity *IncomeEntity) fromDomainObject(do *domain.Income) {
    entity.Id = db_gorm.ID(do.Id)
    entity.Name = do.Name
}
`
		fmt.Println(buffer.String())
		if err := testutils.ExpectMultiLineString(expectString, buffer.String()); err != nil {
			t.Error(err)
		}
	})
}
