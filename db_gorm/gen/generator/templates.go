package generator

const baseTemplate = `// Package {{.PackageName}}.  Code is generated. DO NOT EDIT.
package {{.PackageName}}
`

type baseTemplateData struct {
	PackageName string
}

type objectMapperField struct {
	DomainField       string
	EntityField       string
	Entity2DomainFunc string
	Domain2EntityFunc string
	NeedConvert       bool
}

type objectMapperData struct {
	EntityName       string
	DomainObjectCode string
	FieldMap         []objectMapperField
}

const objectMapper = `func (entity *{{.EntityName}}) toDomainObject() *{{.DomainObjectCode}} {
    return &{{.DomainObjectCode}}{ {{- range .FieldMap }}
        {{- if .NeedConvert }}
        {{.DomainField}}: {{.Entity2DomainFunc}}(entity.{{.EntityField}}),
        {{- else }}
        {{.DomainField}}: entity.{{.EntityField}},
        {{- end }}
        {{- end }}
    }
}

func (entity *{{.EntityName}}) fromDomainObject(do *{{.DomainObjectCode}}) { {{- range .FieldMap }}
        {{- if .NeedConvert }}
    entity.{{.EntityField}} = {{.Domain2EntityFunc}}(do.{{.DomainField}})
        {{- else }}
    entity.{{.EntityField}} = do.{{.DomainField}}
        {{- end }}
{{- end}}
}
`
