package generator

import (
	"io"
	"strings"
	"testing"

	"gitee.com/lipore/plume/db_gorm/gen/code"
	"gitee.com/lipore/plume/db_gorm/gen/spec"
)

var (
	idField = code.StructField{
		Name: "ID",
		Type: code.ExternalType{PackageAlias: "primitive", Name: "ObjectID"},
		Tags: map[string][]string{"bson": {"_id", "omitempty"}},
	}
	genderField = code.StructField{
		Name: "Gender",
		Type: code.SimpleType("Gender"),
		Tags: map[string][]string{"bson": {"gender"}},
	}
	ageField = code.StructField{
		Name: "Age",
		Type: code.SimpleType("int"),
		Tags: map[string][]string{"bson": {"age"}},
	}
)

type mockDbRepositoryGenerator struct {
}

func (rg *mockDbRepositoryGenerator) GenerateConstructor(buffer io.Writer) error {
	buffer.Write([]byte("//placeholder for constructor\n"))
	return nil
}
func (rg *mockDbRepositoryGenerator) GenerateMethod(methodSpec spec.MethodSpec, buffer io.Writer) error {
	buffer.Write([]byte("//placeholder for method\n"))
	return nil
}

func TestGenerateMongoRepository(t *testing.T) {
	userModel := code.Struct{
		Name: "UserModel",
		Fields: code.StructFields{
			idField,
			code.StructField{
				Name: "Username",
				Type: code.SimpleType("string"),
				Tags: map[string][]string{"bson": {"username"}},
			},
			genderField,
			ageField,
		},
	}
	userDomainModel := code.Struct{
		Name: "User",
		Fields: code.StructFields{
			idField,
			code.StructField{
				Name: "Username",
				Type: code.SimpleType("string"),
				Tags: map[string][]string{},
			},
			genderField,
			ageField,
		},
	}
	methods := []spec.MethodSpec{
		// test find: One mode
		{
			Name: "FindByID",
			Params: []code.Param{
				{Name: "ctx", Type: code.ExternalType{PackageAlias: "context", Name: "Context"}},
				{Name: "id", Type: code.ExternalType{PackageAlias: "primitive", Name: "ObjectID"}},
			},
			Returns: []code.Type{code.PointerType{ContainedType: code.SimpleType("UserModel")}, code.SimpleType("error")},
			Operation: spec.FindOperation{
				Mode: spec.QueryModeOne,
				Query: spec.QuerySpec{
					Predicates: []spec.Predicate{
						{FieldReference: spec.FieldReference{idField}, Comparator: spec.ComparatorEqual, ParamIndex: 1},
					},
				},
			},
		},
		// test find: Many mode, And operator, NOT and LessThan comparator
		{
			Name: "FindByGenderNotAndAgeLessThan",
			Params: []code.Param{
				{Name: "ctx", Type: code.ExternalType{PackageAlias: "context", Name: "Context"}},
				{Name: "gender", Type: code.SimpleType("Gender")},
				{Name: "age", Type: code.SimpleType("int")},
			},
			Returns: []code.Type{
				code.PointerType{ContainedType: code.SimpleType("UserModel")},
				code.SimpleType("error"),
			},
			Operation: spec.FindOperation{
				Mode: spec.QueryModeMany,
				Query: spec.QuerySpec{
					Operator: spec.OperatorAnd,
					Predicates: []spec.Predicate{
						{FieldReference: spec.FieldReference{genderField}, Comparator: spec.ComparatorNot, ParamIndex: 1},
						{FieldReference: spec.FieldReference{ageField}, Comparator: spec.ComparatorLessThan, ParamIndex: 2},
					},
				},
			},
		},
		{
			Name: "FindByAgeLessThanEqualOrderByAge",
			Params: []code.Param{
				{Name: "ctx", Type: code.ExternalType{PackageAlias: "context", Name: "Context"}},
				{Name: "age", Type: code.SimpleType("int")},
			},
			Returns: []code.Type{
				code.ArrayType{ContainedType: code.PointerType{ContainedType: code.SimpleType("UserModel")}},
				code.SimpleType("error"),
			},
			Operation: spec.FindOperation{
				Mode: spec.QueryModeMany,
				Query: spec.QuerySpec{
					Predicates: []spec.Predicate{
						{FieldReference: spec.FieldReference{ageField}, Comparator: spec.ComparatorLessThanEqual, ParamIndex: 1},
					},
				},
				Sorts: []spec.Sort{
					{FieldReference: spec.FieldReference{ageField}, Ordering: spec.OrderingAscending},
				},
			},
		},
		{
			Name: "FindByAgeGreaterThanOrderByAgeAsc",
			Params: []code.Param{
				{Name: "ctx", Type: code.ExternalType{PackageAlias: "context", Name: "Context"}},
				{Name: "age", Type: code.SimpleType("int")},
			},
			Returns: []code.Type{
				code.ArrayType{ContainedType: code.PointerType{ContainedType: code.SimpleType("UserModel")}},
				code.SimpleType("error"),
			},
			Operation: spec.FindOperation{
				Mode: spec.QueryModeMany,
				Query: spec.QuerySpec{
					Predicates: []spec.Predicate{
						{FieldReference: spec.FieldReference{ageField}, Comparator: spec.ComparatorGreaterThan, ParamIndex: 1},
					},
				},
				Sorts: []spec.Sort{
					{FieldReference: spec.FieldReference{ageField}, Ordering: spec.OrderingAscending},
				},
			},
		},
		{
			Name: "FindByAgeGreaterThanEqualOrderByAgeDesc",
			Params: []code.Param{
				{Name: "ctx", Type: code.ExternalType{PackageAlias: "context", Name: "Context"}},
				{Name: "age", Type: code.SimpleType("int")},
			},
			Returns: []code.Type{
				code.ArrayType{ContainedType: code.PointerType{ContainedType: code.SimpleType("UserModel")}},
				code.SimpleType("error"),
			},
			Operation: spec.FindOperation{
				Mode: spec.QueryModeMany,
				Query: spec.QuerySpec{
					Predicates: []spec.Predicate{
						{FieldReference: spec.FieldReference{ageField}, Comparator: spec.ComparatorGreaterThanEqual, ParamIndex: 1},
					},
				},
				Sorts: []spec.Sort{
					{FieldReference: spec.FieldReference{ageField}, Ordering: spec.OrderingDescending},
				},
			},
		},
		{
			Name: "FindByAgeBetween",
			Params: []code.Param{
				{Name: "ctx", Type: code.ExternalType{PackageAlias: "context", Name: "Context"}},
				{Name: "fromAge", Type: code.SimpleType("int")},
				{Name: "toAge", Type: code.SimpleType("int")},
			},
			Returns: []code.Type{
				code.ArrayType{ContainedType: code.PointerType{ContainedType: code.SimpleType("UserModel")}},
				code.SimpleType("error"),
			},
			Operation: spec.FindOperation{
				Mode: spec.QueryModeMany,
				Query: spec.QuerySpec{
					Predicates: []spec.Predicate{
						{FieldReference: spec.FieldReference{ageField}, Comparator: spec.ComparatorBetween, ParamIndex: 1},
					},
				},
			},
		},
		{
			Name: "FindByGenderOrAge",
			Params: []code.Param{
				{Name: "ctx", Type: code.ExternalType{PackageAlias: "context", Name: "Context"}},
				{Name: "gender", Type: code.SimpleType("Gender")},
				{Name: "age", Type: code.SimpleType("int")},
			},
			Returns: []code.Type{
				code.ArrayType{ContainedType: code.PointerType{ContainedType: code.SimpleType("UserModel")}},
				code.SimpleType("error"),
			},
			Operation: spec.FindOperation{
				Mode: spec.QueryModeMany,
				Query: spec.QuerySpec{
					Operator: spec.OperatorOr,
					Predicates: []spec.Predicate{
						{FieldReference: spec.FieldReference{genderField}, Comparator: spec.ComparatorEqual, ParamIndex: 1},
						{FieldReference: spec.FieldReference{ageField}, Comparator: spec.ComparatorEqual, ParamIndex: 2},
					},
				},
			},
		},
	}

	code, err := GenerateRepository("user", "domain", userModel, userDomainModel, "UserRepository", methods, &mockDbRepositoryGenerator{}, true, true)

	if err != nil {
		t.Error(err)
	}
	expectedCode := `// Package user.  Code is generated. DO NOT EDIT.
package user

//placeholder for constructor
func (entity *UserModel) toDomainObject() *User {
	return &User{
		ID:       entity.ID,
		Username: entity.Username,
		Gender:   entity.Gender,
		Age:      entity.Age,
	}
}

func (entity *UserModel) fromDomainObject(do *User) {
	entity.ID = do.ID
	entity.Username = do.Username
	entity.Gender = do.Gender
	entity.Age = do.Age
}

//placeholder for method
//placeholder for method
//placeholder for method
//placeholder for method
//placeholder for method
//placeholder for method
//placeholder for method
`
	expectedCodeLines := strings.Split(expectedCode, "\n")
	actualCodeLines := strings.Split(code, "\n")

	for i, line := range expectedCodeLines {
		if line != actualCodeLines[i] {
			t.Errorf("On line %d\nExpected = %v\nActual = %v", i, line, actualCodeLines[i])
		}
	}
}
