package domain

import (
	"gitee.com/lipore/plume/trx"
	"time"
)

type IncomeRepository interface {
	Insert(ctx trx.Context, income *Income) (*Income, error)
	InsertAll(ctx trx.Context, incomes []*Income) ([]*Income, error)
	UpdateById(ctx trx.Context, income *Income, id int64) (bool, error)
	UpdateAccountIdAndDetailByTransactionAt(ctx trx.Context, accountId int64, detail string, transactionAt time.Time) (int64, error)
	DeleteById(ctx trx.Context, id int64) (bool, error)
	DeleteByAccountId(ctx trx.Context, accountId int64) (int64, error)
	FindByAccountIdAndTransactionAtBetween(ctx trx.Context, accountId int64, start, end time.Time) ([]*Income, error)
	FindByAccountIdOrderByAccountIdAndTransactionAtDesc(ctx trx.Context, accountId int64) ([]*Income, error)
	FindById(ctx trx.Context, id int64) (*Income, error)
	CountByAccountId(ctx trx.Context, accountId int64) (int64, error)
	Test(ctx trx.Context) int64
}

var incomeRepository IncomeRepository

func Setup(ir IncomeRepository) {
	incomeRepository = ir
}
