package domain

import (
	"time"
)

type Operator struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}
type Income struct {
	Id            int64     `json:"id"`
	Detail        string    `json:"detail"`
	Subject       Subject   `json:"subject_id"`
	Amount        int64     `json:"amount"`
	AccountId     int64     `json:"account_id"`
	TransactionAt time.Time `json:"transaction_at"`
	CreateAt      time.Time `json:"create_at"`
	Operator      Operator  `json:"operator"`
}

type Subject struct {
	Id int64
}

//func (i *Income) Save(ctx context.Context) error {
//	tx, _, autoCommit, err := trx.GetTransaction(ctx)
//	if err != nil {
//		return err
//	}
//	defer autoCommit()
//	incomeRepository, err := repositoryManager.GetIncomeRepository(tx)
//	if err != nil {
//		return err
//	}
//	if i.Id != 0 {
//		return incomeRepository.Update(i)
//	}
//	return incomeRepository.Create(i)
//}
//
//func (i *Income) Delete(ctx context.Context) error {
//	if i.Id == 0 {
//		return nil
//	}
//	tx, _, autoCommit, err := trx.GetTransaction(ctx)
//	if err != nil {
//		return err
//	}
//	defer autoCommit()
//	incomeRepository, err := repositoryManager.GetIncomeRepository(tx)
//	if err != nil {
//		return err
//	}
//	return incomeRepository.Delete(i.Id)
//}
//
//func ListIncomes(start, end time.Time, accountId int64, ctx context.Context) ([]*Income, error) {
//	tx, _, autoCommit, err := trx.GetTransaction(ctx)
//	if err != nil {
//		return nil, err
//	}
//	defer autoCommit()
//	incomeRepository, err := repositoryManager.GetIncomeRepository(tx)
//	if err != nil {
//		return nil, err
//	}
//	incomes := incomeRepository.FindIncomesByTransactionTimeRange(accountId, start, end)
//	return incomes, nil
//}
