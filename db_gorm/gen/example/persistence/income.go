package persistence

import (
	"gitee.com/lipore/plume/trx"
	"time"

	"gitee.com/lipore/plume/db_gorm/gen/example/domain"

	"gitee.com/lipore/plume/db_gorm"
)

type IncomeEntity struct {
	Id            db_gorm.ID `gorm:"primaryKey"`
	Detail        string     `gorm:"detail;type:VARCHAR(64) NOT NULL"`
	Amount        int64      `gorm:"type:bigint NOT NULL DEFAULT 0"`
	SubjectId     db_gorm.ID `gorm:"not null" mapper:"Subject,subject2Id,id2Subject"`
	AccountId     db_gorm.ID `gorm:"not null"`
	OperatorId    db_gorm.ID `gorm:"not null"`
	TransactionAt time.Time  `gorm:"not null"`
	CreateAt      time.Time  `gorm:"autoCreateTime;not null"`
}

func (entity *IncomeEntity) TableName() string {
	return "income"
}

func subject2Id(subject domain.Subject) db_gorm.ID {
	return db_gorm.ID(subject.Id)
}

func id2Subject(id db_gorm.ID) domain.Subject {
	return domain.Subject{
		Id: int64(id),
	}
}

type IncomeRepository struct {
}

func NewIncomeRepository() domain.IncomeRepository {
	return &IncomeRepository{}
}

func (repo *IncomeRepository) Test(ctx trx.Context) int64 {
	return 1
}
