// Package persistence.  Code is generated. DO NOT EDIT.
package persistence

import (
	"errors"
	"time"

	"gitee.com/lipore/plume/db_gorm"
	"gitee.com/lipore/plume/db_gorm/gen/example/domain"
	"gitee.com/lipore/plume/trx"
	"gorm.io/gorm"
)

func (entity *IncomeEntity) toDomainObject() *domain.Income {
	return &domain.Income{
		Id:            int64(entity.Id),
		Detail:        entity.Detail,
		Amount:        entity.Amount,
		Subject:       id2Subject(entity.SubjectId),
		AccountId:     int64(entity.AccountId),
		TransactionAt: entity.TransactionAt,
		CreateAt:      entity.CreateAt,
	}
}

func (entity *IncomeEntity) fromDomainObject(do *domain.Income) {
	entity.Id = db_gorm.ID(do.Id)
	entity.Detail = do.Detail
	entity.Amount = do.Amount
	entity.SubjectId = subject2Id(do.Subject)
	entity.AccountId = db_gorm.ID(do.AccountId)
	entity.TransactionAt = do.TransactionAt
	entity.CreateAt = do.CreateAt
}

func (repository *IncomeRepository) Insert(ctx trx.Context, income *domain.Income) (*domain.Income, error) {
	entity := &IncomeEntity{}
	entity.fromDomainObject(income)
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return nil, err
	}
	err = conn.Transaction(func(tx *gorm.DB) error {
		return tx.Create(&entity).Error
	})
	if err != nil {
		return nil, err
	}
	return entity.toDomainObject(), nil
}
func (repository *IncomeRepository) InsertAll(ctx trx.Context, incomes []*domain.Income) ([]*domain.Income, error) {
	entities := make([]IncomeEntity, len(incomes))
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return nil, err
	}
	var dos []*domain.Income
	err = conn.Transaction(func(tx *gorm.DB) error {
		for i, d := range incomes {
			entities[i].fromDomainObject(d)
		}
		dos = make([]*domain.Income, len(entities))
		for i, e := range entities {
			err := conn.Create(&e).Error
			if err != nil {
				return err
			}
			dos[i] = e.toDomainObject()
		}
		return err
	})
	if err != nil {
		return nil, err
	}
	return dos, nil
}
func (repository *IncomeRepository) UpdateById(ctx trx.Context, income *domain.Income, id int64) (bool, error) {
	entity := &IncomeEntity{}
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return false, err
	}
	var result *gorm.DB
	err = conn.Transaction(func(tx *gorm.DB) error {
		entity.fromDomainObject(income)
		result = tx.Where(" id = ? ", id).Updates(&entity)
		return result.Error
	})
	if err != nil {
		return false, err
	}

	return result.RowsAffected > 0, nil
}
func (repository *IncomeRepository) UpdateAccountIdAndDetailByTransactionAt(ctx trx.Context, accountId int64, detail string, transactionAt time.Time) (int64, error) {
	entity := &IncomeEntity{}
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return 0, err
	}
	var result *gorm.DB
	err = conn.Transaction(func(tx *gorm.DB) error {
		result = tx.Model(&entity).Where(" transaction_at = ? ", transactionAt).Updates(map[string]interface{}{"account_id": accountId, "detail": detail})
		return result.Error

	})
	if err != nil {
		return 0, err
	}

	return result.RowsAffected, nil
}
func (repository *IncomeRepository) DeleteById(ctx trx.Context, id int64) (bool, error) {
	entity := &IncomeEntity{}
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return false, err
	}
	var result *gorm.DB
	err = conn.Transaction(func(tx *gorm.DB) error {
		result = conn.Where(" id = ? ", id).Delete(&entity)
		return result.Error
	})
	if err != nil {
		return false, err
	}
	return result.RowsAffected > 0, nil
}
func (repository *IncomeRepository) DeleteByAccountId(ctx trx.Context, accountId int64) (int64, error) {
	entity := &IncomeEntity{}
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return 0, err
	}
	var result *gorm.DB
	err = conn.Transaction(func(tx *gorm.DB) error {
		result = conn.Where(" account_id = ? ", accountId).Delete(&entity)
		return result.Error
	})
	if err != nil {
		return 0, err
	}
	return result.RowsAffected, nil
}
func (repository *IncomeRepository) FindByAccountIdAndTransactionAtBetween(ctx trx.Context, accountId int64, start time.Time, end time.Time) ([]*domain.Income, error) {
	entities := make([]IncomeEntity, 0)
	domainObjects := make([]*domain.Income, 0)
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return nil, err
	}
	err = conn.Where(" account_id = ? AND transaction_at BETWEEN ? AND ? ", accountId, start, end).Find(&entities).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return domainObjects, nil
		}
		return nil, err
	}
	for _, e := range entities {
		domainObjects = append(domainObjects, e.toDomainObject())
	}
	return domainObjects, nil
}
func (repository *IncomeRepository) FindByAccountIdOrderByAccountIdAndTransactionAtDesc(ctx trx.Context, accountId int64) ([]*domain.Income, error) {
	entities := make([]IncomeEntity, 0)
	domainObjects := make([]*domain.Income, 0)
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return nil, err
	}
	err = conn.Where(" account_id = ? ", accountId).Order("account_id ASC").Order("transaction_at DESC").Find(&entities).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return domainObjects, nil
		}
		return nil, err
	}
	for _, e := range entities {
		domainObjects = append(domainObjects, e.toDomainObject())
	}
	return domainObjects, nil
}
func (repository *IncomeRepository) FindById(ctx trx.Context, id int64) (*domain.Income, error) {
	e := &IncomeEntity{}
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return nil, err
	}
	err = conn.Where(" id = ? ", id).First(&e).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}
	return e.toDomainObject(), nil
}
func (repository *IncomeRepository) CountByAccountId(ctx trx.Context, accountId int64) (int64, error) {
	var count int64
	conn, err := db_gorm.LoadTx(ctx)
	if err != nil {
		return 0, err
	}
	err = conn.Model(&IncomeEntity{}).Where(" account_id = ? ", accountId).Count(&count).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return 0, nil
		}
		return 0, err
	}
	return count, nil
}
