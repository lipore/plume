package persistence

import "gitee.com/lipore/plume/db_gorm/gen/example/domain"

func init() {
	domain.Setup(NewIncomeRepository())
}
