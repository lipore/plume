DB Code generator
=================

This package is used to generate the repository code for relation database.

## Usage

The repository code can be generated by the following code.

`db-code-genv2 -domain ./dbv2/gen/example/domain -persist ./dbv2/gen/example/persistence -model Income -repo IncomeRepository"`

- **domain** is the path of the domain package, which stores the domain struct and the repository interface.
- **persist** is the path of the persistence package, which stores the entity and the repositoy.
- **model** is the name of the domain struct.
- **repo** is the name of the name of the repository interface.


## For developer

- The code package is used to extrat component from exist code. It will parse structs, interfaces and methods exist.
- The generator is the controller to generate code.
- The spec will consume the structs, interfaces and methods extracted in step 1, and create the specifices for generate code.
- The gorm package is specific generator for generate code for gorm.

## References

[1] [repogen](https://github.com/pgaskin/repogen)