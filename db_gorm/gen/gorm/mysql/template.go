package mysql

type constructorData struct {
	repositoryMethodData
	RepositoryName string
	RepositoryType string
}

var constructorTemplate = `type {{.RepositoryName}} struct {
}

func New{{.RepositoryName}}() {{.RepositoryType}} {
	return &{{.RepositoryName}}{}
}

`
