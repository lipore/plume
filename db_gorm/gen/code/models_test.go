package code

import (
	"reflect"
	"testing"
)

func TestStructsByName(t *testing.T) {
	userStruct := Struct{
		Name: "UserModel",
		Fields: StructFields{
			StructField{Name: "ID", Type: ExternalType{PackageAlias: "primitive", Name: "ObjectID"}},
			StructField{Name: "Username", Type: SimpleType("string")},
		},
	}
	structs := Structs{userStruct}

	t.Run("struct found", func(t *testing.T) {
		structModel, ok := structs.ByName("UserModel")

		if !ok {
			t.Fail()
		}
		if !reflect.DeepEqual(structModel, userStruct) {
			t.Errorf("Expected = %+v\nReceived = %+v", userStruct, structModel)
		}
	})

	t.Run("struct not found", func(t *testing.T) {
		_, ok := structs.ByName("ProductModel")

		if ok {
			t.Fail()
		}
	})
}

func TestStructFieldsByName(t *testing.T) {
	idField := StructField{Name: "ID", Type: ExternalType{PackageAlias: "primitive", Name: "ObjectID"}}
	usernameField := StructField{Name: "Username", Type: SimpleType("string")}
	fields := StructFields{idField, usernameField}

	t.Run("struct field found", func(t *testing.T) {
		field, ok := fields.ByName("Username")

		if !ok {
			t.Fail()
		}
		if !reflect.DeepEqual(field, usernameField) {
			t.Errorf("Expected = %+v\nReceived = %+v", usernameField, field)
		}
	})

	t.Run("struct field not found", func(t *testing.T) {
		_, ok := fields.ByName("Password")

		if ok {
			t.Fail()
		}
	})
}

func TestInterfacesByName(t *testing.T) {
	userRepoIntf := InterfaceType{Name: "UserRepository"}
	interfaces := Interfaces{userRepoIntf}

	t.Run("struct field found", func(t *testing.T) {
		intf, ok := interfaces.ByName("UserRepository")

		if !ok {
			t.Fail()
		}
		if !reflect.DeepEqual(intf, userRepoIntf) {
			t.Errorf("Expected = %+v\nReceived = %+v", userRepoIntf, intf)
		}
	})

	t.Run("struct field not found", func(t *testing.T) {
		_, ok := interfaces.ByName("Password")

		if ok {
			t.Fail()
		}
	})
}

type TypeCodeTestCase struct {
	Name         string
	Type         Type
	ExpectedCode string
}

func TestTypeCode(t *testing.T) {
	testTable := []TypeCodeTestCase{
		{
			Name:         "simple type",
			Type:         SimpleType("UserModel"),
			ExpectedCode: "UserModel",
		},
		{
			Name:         "external type",
			Type:         ExternalType{PackageAlias: "context", Name: "Context"},
			ExpectedCode: "context.Context",
		},
		{
			Name:         "pointer type",
			Type:         PointerType{ContainedType: SimpleType("UserModel")},
			ExpectedCode: "*UserModel",
		},
		{
			Name:         "array type",
			Type:         ArrayType{ContainedType: SimpleType("UserModel")},
			ExpectedCode: "[]UserModel",
		},
		{
			Name: "map type",
			Type: MapType{
				KeyType:   ExternalType{PackageAlias: "primitive", Name: "ObjectID"},
				ValueType: PointerType{ContainedType: SimpleType("UserModel")},
			},
			ExpectedCode: "map[primitive.ObjectID]*UserModel",
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.Name, func(t *testing.T) {
			code := testCase.Type.Code()

			if code != testCase.ExpectedCode {
				t.Errorf("Expected = %+v\nReceived = %+v", testCase.ExpectedCode, code)
			}
		})
	}
}
