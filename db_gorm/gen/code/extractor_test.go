package code

import (
	"go/parser"
	"go/token"
	"reflect"
	"testing"
)

type TestCase struct {
	Name           string
	Source         string
	ExpectedOutput File
}

func TestExtractComponents(t *testing.T) {
	testTable := []TestCase{
		{
			Name:   "package name",
			Source: `package user`,
			ExpectedOutput: File{
				PackageName: "user",
			},
		},
		{
			Name: "single line imports",
			Source: `package user

import ctx "context"
import "go.mongodb.org/mongo-driver/bson/primitive"`,
			ExpectedOutput: File{
				PackageName: "user",
				Imports: []Import{
					{Name: "ctx", Path: "context"},
					{Path: "go.mongodb.org/mongo-driver/bson/primitive"},
				},
			},
		},
		{
			Name: "multiple line imports",
			Source: `package user

import (
	ctx "context"
	"go.mongodb.org/mongo-driver/bson/primitive"
)`,
			ExpectedOutput: File{
				PackageName: "user",
				Imports: []Import{
					{Name: "ctx", Path: "context"},
					{Path: "go.mongodb.org/mongo-driver/bson/primitive"},
				},
			},
		},
		{
			Name: "struct declaration",
			Source: `package user

type UserModel struct {
	ID       primitive.ObjectID ` + "`bson:\"_id,omitempty\" json:\"id\"`" + `
	Username string             ` + "`bson:\"username\" json:\"username\"`" + `
}`,
			ExpectedOutput: File{
				PackageName: "user",
				Structs: Structs{
					Struct{
						Name: "UserModel",
						Fields: StructFields{
							StructField{
								Name: "ID",
								Type: ExternalType{PackageAlias: "primitive", Name: "ObjectID"},
								Tags: map[string][]string{
									"bson": {"_id", "omitempty"},
									"json": {"id"},
								},
							},
							StructField{
								Name: "Username",
								Type: SimpleType("string"),
								Tags: map[string][]string{
									"bson": {"username"},
									"json": {"username"},
								},
							},
						},
					},
				},
			},
		},
		{
			Name: "interface declaration",
			Source: `package user

type UserRepository interface {
	FindOneByID(ctx context.Context, id primitive.ObjectID) (*UserModel, error)
	FindAll(context.Context) ([]*UserModel, error)
	FindByAgeBetween(ctx context.Context, fromAge, toAge int) ([]*UserModel, error)
	InsertOne(ctx context.Context, user *UserModel) (interface{}, error)
	UpdateAgreementByID(ctx context.Context, agreement map[string]bool, id primitive.ObjectID) (bool, error)
	// CustomMethod does custom things.
	CustomMethod(interface {
		Run(arg1 int)
	}) interface {
		Do(arg2 string)
	}
}`,
			ExpectedOutput: File{
				PackageName: "user",
				Interfaces: Interfaces{
					InterfaceType{
						Name: "UserRepository",
						Methods: []Method{
							{
								Name: "FindOneByID",
								Params: []Param{
									{Name: "ctx", Type: ExternalType{PackageAlias: "context", Name: "Context"}},
									{Name: "id", Type: ExternalType{PackageAlias: "primitive", Name: "ObjectID"}},
								},
								Returns: []Type{
									PointerType{ContainedType: SimpleType("UserModel")},
									SimpleType("error"),
								},
							},
							{
								Name: "FindAll",
								Params: []Param{
									{Type: ExternalType{PackageAlias: "context", Name: "Context"}},
								},
								Returns: []Type{
									ArrayType{ContainedType: PointerType{ContainedType: SimpleType("UserModel")}},
									SimpleType("error"),
								},
							},
							{
								Name: "FindByAgeBetween",
								Params: []Param{
									{Name: "ctx", Type: ExternalType{PackageAlias: "context", Name: "Context"}},
									{Name: "fromAge", Type: SimpleType("int")},
									{Name: "toAge", Type: SimpleType("int")},
								},
								Returns: []Type{
									ArrayType{ContainedType: PointerType{ContainedType: SimpleType("UserModel")}},
									SimpleType("error"),
								},
							},
							{
								Name: "InsertOne",
								Params: []Param{
									{Name: "ctx", Type: ExternalType{PackageAlias: "context", Name: "Context"}},
									{Name: "user", Type: PointerType{ContainedType: SimpleType("UserModel")}},
								},
								Returns: []Type{
									InterfaceType{},
									SimpleType("error"),
								},
							},
							{
								Name: "UpdateAgreementByID",
								Params: []Param{
									{Name: "ctx", Type: ExternalType{PackageAlias: "context", Name: "Context"}},
									{Name: "agreement", Type: MapType{KeyType: SimpleType("string"), ValueType: SimpleType("bool")}},
									{Name: "id", Type: ExternalType{PackageAlias: "primitive", Name: "ObjectID"}},
								},
								Returns: []Type{
									SimpleType("bool"),
									SimpleType("error"),
								},
							},
							{
								Name:     "CustomMethod",
								Comments: []string{"CustomMethod does custom things."},
								Params: []Param{
									{
										Type: InterfaceType{
											Methods: []Method{
												{
													Name: "Run",
													Params: []Param{
														{Name: "arg1", Type: SimpleType("int")},
													},
												},
											},
										},
									},
								},
								Returns: []Type{
									InterfaceType{
										Methods: []Method{
											{
												Name: "Do",
												Params: []Param{
													{Name: "arg2", Type: SimpleType("string")},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			Name: "integration",
			Source: `package user

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserModel struct {
	ID       primitive.ObjectID ` + "`bson:\"_id,omitempty\" json:\"id\"`" + `
	Username string             ` + "`bson:\"username\" json:\"username\"`" + `
}

type UserRepository interface {
	FindOneByID(ctx context.Context, id primitive.ObjectID) (*UserModel, error)
	FindAll(ctx context.Context) ([]*UserModel, error)
}
`,
			ExpectedOutput: File{
				PackageName: "user",
				Imports: []Import{
					{Path: "context"},
					{Path: "go.mongodb.org/mongo-driver/bson/primitive"},
				},
				Structs: Structs{
					Struct{
						Name: "UserModel",
						Fields: StructFields{
							StructField{
								Name: "ID",
								Type: ExternalType{PackageAlias: "primitive", Name: "ObjectID"},
								Tags: map[string][]string{
									"bson": {"_id", "omitempty"},
									"json": {"id"},
								},
							},
							StructField{
								Name: "Username",
								Type: SimpleType("string"),
								Tags: map[string][]string{
									"bson": {"username"},
									"json": {"username"},
								},
							},
						},
					},
				},
				Interfaces: Interfaces{
					InterfaceType{
						Name: "UserRepository",
						Methods: []Method{
							{
								Name: "FindOneByID",
								Params: []Param{
									{Name: "ctx", Type: ExternalType{PackageAlias: "context", Name: "Context"}},
									{Name: "id", Type: ExternalType{PackageAlias: "primitive", Name: "ObjectID"}},
								},
								Returns: []Type{
									PointerType{ContainedType: SimpleType("UserModel")},
									SimpleType("error"),
								},
							},
							{
								Name: "FindAll",
								Params: []Param{
									{Name: "ctx", Type: ExternalType{PackageAlias: "context", Name: "Context"}},
								},
								Returns: []Type{
									ArrayType{ContainedType: PointerType{ContainedType: SimpleType("UserModel")}},
									SimpleType("error"),
								},
							},
						},
					},
				},
			},
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.Name, func(t *testing.T) {
			fset := token.NewFileSet()
			f, _ := parser.ParseFile(fset, "", testCase.Source, parser.ParseComments)

			file := ExtractComponents(f)

			if !reflect.DeepEqual(file, testCase.ExpectedOutput) {
				t.Errorf("Expected = %+v\nReceived = %+v", testCase.ExpectedOutput, file)
			}
		})
	}
}
