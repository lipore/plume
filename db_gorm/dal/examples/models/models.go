package models

import (
	"context"

	"gitee.com/lipore/plume/db_gorm"
)

// @dal:name ResourceOwner
type UserDo interface {
	db_gorm.BaseCrud
	FindNameByValue(context.Context, int64) (string, error)
}
