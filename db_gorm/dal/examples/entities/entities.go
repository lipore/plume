package entities

// @dal:name ResourceOwner
type ResourceOwnerEntity struct {
	Id    int64
	Name  string
	Value int64
}
