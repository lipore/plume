package models

import (
	"strings"
	"unicode"
)

type Condition interface {
}

type FieldCondition struct {
	Field *FieldInfo
	IsNot bool
}

type BiCondition struct {
	Left     Condition
	Right    Condition
	Operator string
}

// ParseCondition parse condition string to Condition
func ParseCondition(c string) Condition {
	orConditions := strings.Split(c, "Or")
	if len(orConditions) > 1 {
		or := &BiCondition{
			Operator: "Or",
		}
		or.Left = ParseCondition(orConditions[0])
		or.Right = ParseCondition(strings.Join(orConditions[1:], "Or"))
		return or
	}
	andConditions := strings.Split(c, "And")
	if len(andConditions) > 1 {
		and := &BiCondition{
			Operator: "And",
		}
		and.Left = ParseCondition(andConditions[0])
		and.Right = ParseCondition(strings.Join(andConditions[1:], "And"))
		return and
	}
	if strings.HasPrefix(c, "Not") && unicode.IsUpper(rune(c[3])) {
		return &FieldCondition{
			Field: &FieldInfo{
				Name: c[3:],
			},
			IsNot: true,
		}
	}
	return &FieldCondition{
		Field: &FieldInfo{
			Name: c,
		},
		IsNot: false,
	}
}
