package models_test

import (
	"testing"

	"gitee.com/lipore/plume/db_gorm/dal/models"
)

func TestNewAnnotation(t *testing.T) {
	ann := models.NewAnnotation("// @model:Id int64")
	if ann.Name != "Id" {
		t.Error("name should be Id")
	}
	if ann.Value != "int64" {
		t.Error("value should be int64")
	}
	ann = models.NewAnnotation("// abc")
	if ann != nil {
		t.Error("should be nil")
	}
}
