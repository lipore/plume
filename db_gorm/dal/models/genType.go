package models

import "go/ast"

type GenType struct {
	Doc *ast.CommentGroup
	Pkg *Package
}

func NewGenType(pkg *Package) *GenType {
	return &GenType{
		Pkg: pkg,
	}
}

func (m *GenType) Visit(n ast.Node) ast.Visitor {
	switch n := n.(type) {
	case *ast.CommentGroup:
		m.Doc = n
		return nil
	case *ast.TypeSpec:
		typeName := ""
		if n.Name != nil {
			typeName = n.Name.Name
		}
		switch n.Type.(type) {
		case *ast.StructType:
			t := NewStructInfo(typeName, m.Pkg)
			if m.Doc != nil {
				ast.Walk(t, m.Doc)
			}
			m.Pkg.StructInfos[typeName] = t
			return t
		case *ast.InterfaceType:
			t := NewInterfaceInfo(typeName, m.Pkg)
			if m.Doc != nil {
				ast.Walk(t, m.Doc)
			}
			m.Pkg.InterfaceInfos[typeName] = t
			return t
		}
	}
	return m
}
