package models

import "go/ast"

// MethodInfo is the method info
type MethodInfo struct {
	Name     string
	RecvType string // must be nil for interface method
	Params   FieldsInfo
	Results  FieldsInfo
}

func (m *MethodInfo) Visit(n ast.Node) ast.Visitor {
	switch n := n.(type) {
	case *ast.FuncDecl:
		m.Name = n.Name.Name
		if n.Recv != nil && len(n.Recv.List) > 0 {
			m.RecvType = getType(n.Recv.List[0].Type)
		}
		ast.Walk(m, n.Type)
		return nil
	case *ast.FuncType:
		if n.Params != nil {
			ast.Walk(&m.Params, n.Params)
		}
		if n.Results != nil {
			ast.Walk(&m.Results, n.Results)
		}
		return nil
	}
	return m
}

func (m *MethodInfo) Equal(n *MethodInfo) bool {
	if n == nil {
		return false
	}
	if m.Name != n.Name {
		return false
	}
	if len(m.Params) != len(n.Params) {
		return false
	}
	if len(m.Results) != len(n.Results) {
		return false
	}
	for i, param := range m.Params {
		if param.Type != n.Params[i].Type {
			return false
		}
	}
	for i, result := range m.Results {
		if result.Type != n.Results[i].Type {
			return false
		}
	}
	return true
}
