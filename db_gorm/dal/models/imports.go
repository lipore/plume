package models

import (
	"go/ast"
	"path"
	"strconv"
)

type Import struct {
	Name string
	Path string
}

type Imports []*Import

func (i *Import) Visit(n ast.Node) ast.Visitor {
	switch n := n.(type) {
	case *ast.Ident:
		name := n.Name
		if name != "_" && name != "." {
			i.Name = name
		}
		return nil
	case *ast.BasicLit:
		p, _ := strconv.Unquote(n.Value)
		i.Path = p
		if i.Name == "" {
			i.Name = path.Base(p)
		}
		return nil
	}
	return i
}
