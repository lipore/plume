package models_test

import (
	"reflect"
	"testing"

	"gitee.com/lipore/plume/db_gorm/dal/models"
)

func TestParseCondition(t *testing.T) {
	// Test case 1: Test parsing a simple field condition
	t.Run("ParseCondition - FieldCondition", func(t *testing.T) {
		c := "fieldName"
		expected := &models.FieldCondition{
			Field: &models.FieldInfo{
				Name: "fieldName",
			},
			IsNot: false,
		}
		result := models.ParseCondition(c)
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("ParseCondition(%s) = %v, expected %v", c, result, expected)
		}
	})

	// Test case 2.1: Test parsing a negated field condition
	t.Run("ParseCondition - Negated FieldCondition", func(t *testing.T) {
		c := "NotFieldName"
		expected := &models.FieldCondition{
			Field: &models.FieldInfo{
				Name: "FieldName",
			},
			IsNot: true,
		}
		result := models.ParseCondition(c)
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("ParseCondition(%s) = %v, expected %v", c, result, expected)
		}
	})
	// Test case 2.2: Test parsing a fake negated field condition
	t.Run("ParseCondition - Fake Negated FieldCondition", func(t *testing.T) {
		c := "NotifyName"
		expected := &models.FieldCondition{
			Field: &models.FieldInfo{
				Name: "NotifyName",
			},
			IsNot: false,
		}
		result := models.ParseCondition(c)
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("ParseCondition(%s) = %v, expected %v", c, result, expected)
		}
	})

	// Test case 3: Test parsing a bi-condition with "Or" operator
	t.Run("ParseCondition - BiCondition (Or)", func(t *testing.T) {
		c := "fieldName1OrfieldName2"
		expected := &models.BiCondition{
			Operator: "Or",
			Left: &models.FieldCondition{
				Field: &models.FieldInfo{
					Name: "fieldName1",
				},
				IsNot: false,
			},
			Right: &models.FieldCondition{
				Field: &models.FieldInfo{
					Name: "fieldName2",
				},
				IsNot: false,
			},
		}
		result := models.ParseCondition(c)
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("ParseCondition(%s) = %v, expected %v", c, result, expected)
		}
	})

	// Test case 4: Test parsing a bi-condition with "And" operator
	t.Run("ParseCondition - BiCondition (And)", func(t *testing.T) {
		c := "fieldName1AndfieldName2"
		expected := &models.BiCondition{
			Operator: "And",
			Left: &models.FieldCondition{
				Field: &models.FieldInfo{
					Name: "fieldName1",
				},
				IsNot: false,
			},
			Right: &models.FieldCondition{
				Field: &models.FieldInfo{
					Name: "fieldName2",
				},
				IsNot: false,
			},
		}
		result := models.ParseCondition(c)
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("ParseCondition(%s) = %v, expected %v", c, result, expected)
		}
	})

	// Test case 5: Test parsing a bi-condition with "Or" and "And" operators
	t.Run("ParseCondition - BiCondition (Or, And)", func(t *testing.T) {
		c := "fieldName1OrfieldName2AndfieldName3"
		expected := &models.BiCondition{
			Operator: "Or",
			Left: &models.FieldCondition{
				Field: &models.FieldInfo{
					Name: "fieldName1",
				},
				IsNot: false,
			},
			Right: &models.BiCondition{
				Operator: "And",
				Left: &models.FieldCondition{
					Field: &models.FieldInfo{
						Name: "fieldName2",
					},
					IsNot: false,
				},
				Right: &models.FieldCondition{
					Field: &models.FieldInfo{
						Name: "fieldName3",
					},
					IsNot: false,
				},
			},
		}
		result := models.ParseCondition(c)
		if !reflect.DeepEqual(result, expected) {
			t.Errorf("ParseCondition(%s) = %v, expected %v", c, result, expected)
		}
	})
}
