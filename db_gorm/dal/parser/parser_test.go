package parser_test

import (
	"fmt"
	"testing"

	"gitee.com/lipore/plume/db_gorm/dal/parser"
	"github.com/go-playground/assert/v2"
)

func Test_ParsePackage(t *testing.T) {
	r, err := parser.ParsePackage("../examples/models")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, len(r), 1)

	if err != nil {
		t.Fatal(err)
	}

	fmt.Printf("result: %v\n", r[0])
}
