package template

import (
	"io"

	"gitee.com/lipore/plume/db_gorm/dal/models"
)

const DoInterface = `
type {{.Name}} interface {
	{{if .ParentInterface}}{{.ParentInterface.Name}}{{end}}
	{{range .Methods}}
	{{.Name}}({{- range .Params}}{{.Name}} {{.Type}},{{- end}}) ({{- range .Results}}{{.Type}},{{- end}})
	{{end}}
}`

func GenerateDoInterface(w io.Writer, data *models.InterfaceInfo) {
	err := render(DoInterface, w, data)
	if err != nil {
		panic(err)
	}
}

type DoCreatorData struct {
	ModelName      string
	ModelInterface string
}

const DoCreator = `
var {{.ModelName}}Creator func() {{.ModelInterface}}

func Setup{{.ModelName}}Creator(i func() {{.ModelInterface}}) {
	{{.ModelName}}Creator = i
}

func New{{.ModelName}}(){{.ModelInterface}} {
	return {{.ModelName}}Creator()
}`

func GenerateDoCreator(w io.Writer, data *DoCreatorData) {
	err := render(DoCreator, w, data)
	if err != nil {
		panic(err)
	}
}
