package db_gorm_test

import (
	"context"
	"gitee.com/lipore/plume/db_gorm"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLoadTx(t *testing.T) {
	trxMgr, err := db_gorm.Connect(db_gorm.WithDBType(db_gorm.SQLite), db_gorm.WithDsn("gorm.db"))
	trx, err := trxMgr.Start(context.Background())
	assert.NotNil(t, trx)
	assert.Nil(t, err)
	conn, err := db_gorm.LoadTx(trx)
	assert.NotNil(t, conn)
	assert.Nil(t, err)
}
