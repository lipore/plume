package db_gorm_test

import (
	v2 "gitee.com/lipore/plume/db_gorm"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewConfig(t *testing.T) {
	config := v2.NewConfig(v2.WithDBType(v2.SQLite), v2.WithDsn("gorm.db"), v2.WithShowSql(true), v2.WithSlowThreshold(1*time.Second))
	assert.NotNil(t, config)
	assert.Equal(t, v2.SQLite, config.DBType)
	assert.Equal(t, "gorm.db", config.Dsn)
	assert.Equal(t, true, config.ShowSql)
	assert.Equal(t, time.Second, config.SlowThreshold)
}
