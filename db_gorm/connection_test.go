package db_gorm_test

import (
	"gitee.com/lipore/plume/db_gorm"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewClient(t *testing.T) {
	conn, err := db_gorm.NewClient(db_gorm.WithDBType(db_gorm.SQLite), db_gorm.WithDsn("gorm.db"), db_gorm.WithShowSql(true), db_gorm.WithSlowThreshold(1*time.Second))
	if err != nil {
		t.Error(err.Error())
	}
	assert.NotNil(t, conn, "connect expect to be not nil")
}
