package db_gorm

import (
	"context"
	"time"

	"gorm.io/gorm"
)

type Model struct {
	ID        ID
	CreatedAt time.Time
	UpdatedAt time.Time
}

type SoftDeletion struct {
	DeletedAt gorm.DeletedAt
}

type BaseCrud interface {
	Create(context.Context) error
	Update(context.Context) error
	Delete(context.Context) error
	Find(context.Context) error
}
