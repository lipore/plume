package logger

import (
	"fmt"
)

type Options struct {
	ErrorOutputPaths []string
	OutputPaths      []string
	Level            string
	Name             string
	EnableColor      bool
	Format           string
}

const (
	TextFormat = "text"
	JsonFormat = "json"
)

func NewOptions() *Options {
	return &Options{
		ErrorOutputPaths: []string{"stderr"},
		OutputPaths:      []string{"stdout"},
		Level:            INFO.String(),
		EnableColor:      false,
		Format:           TextFormat,
	}
}

func (o *Options) Validate() []error {
	var errs []error

	var level Level
	if err := level.Parse(o.Level); err != nil {
		errs = append(errs, err)
	}

	if TextFormat != o.Format && JsonFormat != o.Format {
		errs = append(errs, fmt.Errorf("not a valid log format: %q", o.Format))
	}

	return errs
}
