package init

import (
	"gitee.com/lipore/plume/logger"
	"gitee.com/lipore/plume/logger/logrus"
)

func New(options *logger.Options) logger.Logger {
	return logrus.NewLogger(options)
}
