package logger

// Debugf logs a debug message with formatting.
func Debugf(format string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Debugf(format, args...)
	}
}

// Infof logs an info message with formatting.
func Infof(format string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Infof(format, args...)
	}
}

// Printf logs a message with formatting.
func Printf(format string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Printf(format, args...)
	}
}

// Warnf logs a warning message with formatting.
func Warnf(format string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warnf(format, args...)
	}
}

// Warningf logs a warning message with formatting.
func Warningf(format string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warningf(format, args...)
	}
}

// Errorf logs an error message with formatting.
func Errorf(format string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Errorf(format, args...)
	}
}

// Fatalf logs a fatal message with formatting.
func Fatalf(format string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Fatalf(format, args...)
	}
}

// Panicf logs a panic message with formatting.
func Panicf(format string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Panicf(format, args...)
	}
}

// Debug logs a debug message.
func Debug(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Debug(args...)
	}
}

// Info logs an info message.
func Info(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Info(args...)
	}
}

// Print logs a message.
func Print(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Print(args...)
	}
}

// Warn logs a warning message.
func Warn(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warn(args...)
	}
}

// Warning logs a warning message.
func Warning(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warning(args...)
	}
}

// Error logs an error message.
func Error(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Error(args...)
	}
}

// Fatal logs a fatal message.
func Fatal(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Fatal(args...)
	}
}

// Panic logs a panic message.
func Panic(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Panic(args...)
	}
}

// Debugln logs a debug message with a line break.
func Debugln(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Debugln(args...)
	}
}

// Infoln logs an info message with a line break.
func Infoln(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Infoln(args...)
	}
}

// Println logs a message with a line break.
func Println(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Println(args...)
	}
}

// Warnln logs a warning message with a line break.
func Warnln(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warnln(args...)
	}
}

// Warningln logs a warning message with a line break.
func Warningln(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warningln(args...)
	}
}

// Errorln logs an error message with a line break.
func Errorln(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Errorln(args...)
	}
}

// Fatalln logs a fatal message with a line break.
func Fatalln(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Fatalln(args...)
	}
}

// Panicln logs a panic message with a line break.
func Panicln(args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Panicln(args...)
	}
}
