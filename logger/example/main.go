package main

import (
	logger2 "gitee.com/lipore/plume/logger"
	initial "gitee.com/lipore/plume/logger/init"
	"os"
)

func main() {
	options := logger2.Options{
		ErrorOutputPaths: []string{"./logs/err.log"},
		OutputPaths:      []string{"./logs/log"},
		Level:            "DEBUG",
		Name:             "test",
		EnableColor:      true,
		Format:           "text",
	}
	logger := initial.New(&options)
	logger.Debug("debug")
	wd, _ := os.Getwd()
	logger.Info("info", "current", wd)
	logger.Warn("warn")
	logger.Error("error")
	//logger.Panic("panic")
	//logger.Fatal("fatal")
}
