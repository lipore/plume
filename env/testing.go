package env

import (
	"os"
	"strings"
)

func IsTesting() bool {
	return strings.HasSuffix(os.Args[0], ".test") || strings.Contains(os.Args[0], "/_test/") || strings.Contains(os.Args[0], "__debug_bin")
}
