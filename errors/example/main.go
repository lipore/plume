package main

import (
	"errors"
	"fmt"
	errors2 "gitee.com/lipore/plume/errors"
)

func main() {
	var err error
	err = errors.New("external error")
	fmt.Printf("%v\n", err)
	err = errors2.WithMessage(errors.New("external error"), "internal error")
	fmt.Printf("%v\n", err)
	fmt.Printf("%+v\n", err)
	err = errors2.WithStack(errors.New("external error"))
	fmt.Printf("%v\n", err)
	fmt.Printf("%+v\n", err)
	err = errors2.WithCode(err, 100123, "internal error")
	fmt.Printf("%%s :\n%s\n", err)
	fmt.Printf("%%v :\n%v\n", err)
	fmt.Printf("%%-v :\n%-v\n", err)
	fmt.Printf("%%+v :\n%+v\n", err)
	fmt.Printf("%%#v :\n%#v\n", err)
	fmt.Printf("%%#-v :\n%#-v\n", err)
	fmt.Printf("%%#+v :\n%#+v\n", err)
}
