package v2

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	Success = "success"
	Error   = "error"
	Status  = "status"
)

func Response(c *gin.Context, obj any) {
	c.Header(Status, string(Success))
	c.JSON(http.StatusOK, obj)
}

type CRUDApi interface {
	Get(c *gin.Context)
	List(c *gin.Context)
	Create(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

func RegisterCRUDApi(route *gin.Engine, pathPrefix string, api CRUDApi, middlewares ...gin.HandlerFunc) {
	route.
		Group(pathPrefix, middlewares...).
		GET("/", api.List).
		POST("/", api.Create).
		GET("/:id", api.Get).
		PUT("/:id", api.Update).
		DELETE("/:id", api.Delete)
}

func JsonResponse(c *gin.Context, httpCode int, code int, obj any) {
	c.Header(Status, string(Success))
	c.JSON(httpCode, gin.H{
		"code": code,
		"data": obj,
	})
}
