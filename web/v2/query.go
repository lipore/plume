package v2

import "net/url"

func EncodeQuery(param map[string]string) string {
	q := url.Values{}
	for k, v := range param {
		q.Add(k, v)
	}
	return q.Encode()
}
