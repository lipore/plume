package v2

import "net/http"

type Middleware interface {
	Apply(next http.Handler) http.Handler
}
