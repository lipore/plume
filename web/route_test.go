package web

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"testing"
)

func TestBuildRegexp(t *testing.T) {
	route := NewRoute()
	mux := http.NewServeMux()
	ra := route.SubRoute("/users/{:userId}")
	ra.Handle("/{:roleId}", mux)
	r := &http.Request{URL: &url.URL{Path: "/users/2/3"}}
	h, pv := route.handler(r.URL.Path)
	if mux != h || pv == nil || pv["userId"] != "2" || pv["roleId"] != "3" {
		t.Fail()
	}
	r = &http.Request{URL: &url.URL{Path: "/users/2/3/"}}
	h, pv = route.handler(r.URL.Path)
	if mux != h || pv == nil || pv["userId"] != "2" || pv["roleId"] != "3" {
		t.Fail()
	}
	r = &http.Request{URL: &url.URL{Path: "/users/2"}}
	h, pv = route.handler(r.URL.Path)
	if mux == h || pv != nil {
		t.Fail()
	}
	r = &http.Request{URL: &url.URL{Path: "/users/2/3/4"}}
	h, pv = route.handler(r.URL.Path)
	if mux == h || pv != nil {
		t.Fail()
	}
}

type writer struct {
	buf bytes.Buffer
}

func (w *writer) Header() http.Header {
	return nil
}

func (w *writer) Write(b []byte) (int, error) {
	return w.buf.Write(b)
}

func (w *writer) WriteHeader(statusCode int) {
}

func mockHandler(response string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(response))
	})
}

func expectResponse(expectResponse string, r *http.Request, handler http.Handler) error {
	w := &writer{buf: *bytes.NewBuffer(nil)}
	handler.ServeHTTP(w, r)
	if w.buf.String() != expectResponse {
		return fmt.Errorf("expect: %s, actual: %s", expectResponse, w.buf.String())
	}
	return nil
}

func TestNestedRoute(t *testing.T) {
	route := NewRoute()

	ra := route.SubRoute("/a/")
	rb := ra.SubRoute("/b/")
	rb.Handle("/", mockHandler("/a/b/"))
	rb.Handle("{:t}", mockHandler("t"))

	r := &http.Request{URL: &url.URL{Path: "/a/b/"}}
	h, _ := route.handler(r.URL.Path)
	if err := expectResponse("/a/b/", r, h); err != nil {
		t.Fail()
	}
	r = &http.Request{URL: &url.URL{Path: "/a/b/1"}}
	h, pv := route.handler(r.URL.Path)
	if err := expectResponse("t", r, h); err != nil || len(pv) == 0 {
		t.Fail()
	}
}
