package web

import (
	"log"
	"net/http"
)

func AccessLogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request %s from %s", r.URL.Path, r.RemoteAddr)
		next.ServeHTTP(w, r)
	})
}
