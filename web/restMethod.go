package web

import "net/http"

type RestMethodRoute struct {
	mr *MethodRoute
}

func (route *RestMethodRoute) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	route.mr.ServeHTTP(w, r)
}

func (route *RestMethodRoute) Handle(method string, f RestFunc) {
	route.mr.Handle(method, RestControllerHandler(f))
}

func (route *RestMethodRoute) GET(f RestFunc) *RestMethodRoute {
	route.mr.GET(RestControllerHandler(f))
	return route
}
func (route *RestMethodRoute) POST(f RestFunc) *RestMethodRoute {
	route.mr.POST(RestControllerHandler(f))
	return route
}
func (route *RestMethodRoute) PUT(f RestFunc) *RestMethodRoute {
	route.mr.PUT(RestControllerHandler(f))
	return route
}
func (route *RestMethodRoute) DELETE(f RestFunc) *RestMethodRoute {
	route.mr.DELETE(RestControllerHandler(f))
	return route
}
