package utils

import "bytes"

// CamelCase convert name to camelCase
func CamelCase(name string) string {
	cc := new(bytes.Buffer)
	firstFlag := true
	underscoreFlag := false
	for _, b := range []byte(name) {
		if firstFlag {
			if b != '_' {
				cc.WriteByte(Lower(b))
				firstFlag = false
			}
			continue
		}
		if b == '_' {
			underscoreFlag = true
			continue
		}
		if underscoreFlag {
			cc.WriteByte(Upper(b))
			underscoreFlag = false
			continue
		}
		cc.WriteByte(b)
	}
	return string(cc.Bytes())
}

func SnakeCase(name string) string {
	cc := new(bytes.Buffer)
	for _, b := range []byte(name) {
		if IsUpperCase(b) {
			cc.WriteByte('_')
			cc.WriteByte(Lower(b))
			continue
		}
		cc.WriteByte(b)
	}
	return string(cc.Bytes())
}

func PascalCase(name string) string {
	s := CamelCase(name)
	byts := []byte(s)
	byts[0] = Upper(byts[0])
	return string(byts)
}

func IsUpperCase(b byte) bool {
	return b >= 'A' && b <= 'Z'
}
func IsLowerCase(b byte) bool {
	return b >= 'a' && b <= 'z'
}
func Upper(b byte) byte {
	if IsLowerCase(b) {
		return b + 'A' - 'a'
	}
	return b
}
func Lower(b byte) byte {
	if IsUpperCase(b) {
		return b - 'A' + 'a'
	}
	return b
}
