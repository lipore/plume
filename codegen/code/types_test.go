package code_test

import (
	"gitee.com/lipore/plume/codegen/code"
	"testing"
)

type TypeCodeTestCase struct {
	Name         string
	Type         code.Type
	ExpectedCode string
}

func TestTypeCode(t *testing.T) {
	testTable := []TypeCodeTestCase{
		{
			Name:         "simple type",
			Type:         code.SimpleType("UserModel"),
			ExpectedCode: "UserModel",
		},
		{
			Name:         "external type",
			Type:         code.ExternalType{PackageAlias: "context", Name: "Context"},
			ExpectedCode: "context.Context",
		},
		{
			Name:         "pointer type",
			Type:         code.PointerType{ContainedType: code.SimpleType("UserModel")},
			ExpectedCode: "*UserModel",
		},
		{
			Name:         "array type",
			Type:         code.ArrayType{ContainedType: code.SimpleType("UserModel")},
			ExpectedCode: "[]UserModel",
		},
		{
			Name: "map type",
			Type: code.MapType{
				KeyType:   code.ExternalType{PackageAlias: "primitive", Name: "ObjectID"},
				ValueType: code.PointerType{ContainedType: code.SimpleType("UserModel")},
			},
			ExpectedCode: "map[primitive.ObjectID]*UserModel",
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.Name, func(t *testing.T) {
			c := testCase.Type.Code()

			if c != testCase.ExpectedCode {
				t.Errorf("Expected = %+v\nReceived = %+v", testCase.ExpectedCode, c)
			}
		})
	}
}
