package code

import (
	"fmt"
	"go/ast"
	"strconv"
)

// Import is a model for package imports
type Import struct {
	Name string
	Path string
}

func ExtraImports(spec *ast.ImportSpec) *Import {
	var imp Import
	if spec.Name != nil {
		imp.Name = spec.Name.Name
	}
	importPath, err := strconv.Unquote(spec.Path.Value)
	if err != nil {
		fmt.Printf("cannot unquote import %s : %s \n", spec.Path.Value, err)
		imp.Path = spec.Path.Value
	} else {
		imp.Path = importPath
	}
	return &imp
}
