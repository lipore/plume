package code_test

import (
	"gitee.com/lipore/plume/codegen/code"
	"go/ast"
	"go/parser"
	"go/token"
	"reflect"
	"testing"
)

func TestExtraFunctions(t *testing.T) {
	file := `package code_test


func a(b int) (int,error) {
}
type C struct{}
func (c *C)b(b int) (int,error) {
}
`
	fset := token.NewFileSet()
	f, _ := parser.ParseFile(fset, "", file, parser.ParseComments)
	funcs := make([]*code.Method, 0)
	for _, decl := range f.Decls {
		if funcDecl, ok := decl.(*ast.FuncDecl); ok {
			funcs = append(funcs, code.ExtractMethod(funcDecl))
		}
	}
	expectFunctions := []*code.Method{
		&code.Method{
			Function: code.Function{
				Name: "a",
				Params: []code.Param{
					{Name: "b", Type: code.SimpleType("int")},
				},
				Returns: []code.Type{code.SimpleType("int"), code.SimpleType("error")},
			},
			Receiver: code.Receiver{},
		},
		&code.Method{
			Function: code.Function{
				Name: "b",
				Params: []code.Param{
					{Name: "b", Type: code.SimpleType("int")},
				},
				Returns: []code.Type{code.SimpleType("int"), code.SimpleType("error")},
			},
			Receiver: code.Receiver{
				Type: code.PointerType{ContainedType: code.SimpleType("C")},
				Name: "c",
			},
		},
	}
	if !reflect.DeepEqual(funcs, expectFunctions) {
		t.Errorf("extract function failed")
	}
}
