package code

// File is a container of all required components for code generation in the file
type File struct {
	PackageName string
	Imports     []*Import
	Structs     Structs
	Interfaces  InterfaceSpecs
	Methods     []*Method
	Constants   ConstantFields
}

type ConstantField struct {
	Name     string
	Type     Type
	Value    any
	Comments []string
}

type ConstantFields []*ConstantField
