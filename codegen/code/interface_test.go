package code_test

import (
	"gitee.com/lipore/plume/codegen/code"
	"go/ast"
	"go/parser"
	"go/token"
	"reflect"
	"testing"
)

func TestExtractInterfaceType(t *testing.T) {
	source := `package i

import "context"

type abc interface{
  a()
  b(c context.Context, d string) (string, error)
}
`

	fset := token.NewFileSet()
	f, _ := parser.ParseFile(fset, "", source, parser.ParseComments)
	interfaces := make([]*code.InterfaceSpec, 0)
	for _, decl := range f.Decls {
		if genDecl, ok := decl.(*ast.GenDecl); ok {
			for _, spec := range genDecl.Specs {
				if tSpec, ok := spec.(*ast.TypeSpec); ok {
					if iType, ok := tSpec.Type.(*ast.InterfaceType); ok {
						interfaces = append(interfaces, code.ExtractInterfaceSpec(tSpec, iType))
					}
				}
			}
		}
	}
}

func TestInterfacesByName(t *testing.T) {
	userRepoIntf := code.InterfaceSpec{Name: "UserRepository"}
	interfaces := code.InterfaceSpecs{&userRepoIntf}

	t.Run("struct field found", func(t *testing.T) {
		intf, ok := interfaces.ByName("UserRepository")

		if !ok {
			t.Fail()
		}
		if !reflect.DeepEqual(intf, &userRepoIntf) {
			t.Errorf("Expected = %+v\nReceived = %+v", userRepoIntf, intf)
		}
	})

	t.Run("struct field not found", func(t *testing.T) {
		_, ok := interfaces.ByName("Password")

		if ok {
			t.Fail()
		}
	})
}
