package code_test

import (
	"gitee.com/lipore/plume/codegen/code"
	"go/ast"
	"go/parser"
	"go/token"
	"reflect"
	"testing"
)

func TestExtraImports(t *testing.T) {
	source := `package test
import (
  "fmt"
  "errors"
  plumeErrors "gitee.com/lipore/plume/errors"
)`
	fset := token.NewFileSet()
	f, _ := parser.ParseFile(fset, "", source, parser.ParseComments)
	imports := make([]*code.Import, 0)
	for _, decl := range f.Decls {
		if genDecl, ok := decl.(*ast.GenDecl); ok {
			for _, spec := range genDecl.Specs {
				if importSpec, ok := spec.(*ast.ImportSpec); ok {
					imports = append(imports, code.ExtraImports(importSpec))
				}
			}
		}
	}

	exceptImports := []*code.Import{
		{Path: "fmt"},
		{Path: "errors"},
		{Name: "plumeErrors", Path: "gitee.com/lipore/plume/errors"},
	}

	if len(imports) != 3 && !reflect.DeepEqual(imports, exceptImports) {
		t.Errorf("failed to extract imports")
	}
}
