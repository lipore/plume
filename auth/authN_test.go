package auth

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/lipore/plume/logger"
	_ "gitee.com/lipore/plume/logger/logrus"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func setup() (*gin.Engine, PublicKeyStore) {
	r := gin.Default()
	authNDs := newInmemoryUserDataSource(map[string]string{"abc": "123", "def": "456"}, map[string][]Access{})
	keyStore := newInThreadPublicKeyStore()
	authNEP := NewNEndpoints(
		context.Background(), keyStore, authNDs, NewNEndpointOptions(),
	)
	r.POST("/login", authNEP.LoginEndpoint)
	r.POST("/refresh", authNEP.RefreshTokenEndpoint)
	return r, keyStore
}

type NTestSuite struct {
	requestBody  *NRequest
	useForm      bool
	expectStatus int
	verifyToken  bool
}

func TestLoginEndpoint(t *testing.T) {
	router, authStrategy := setup()

	tests := []NTestSuite{
		{
			requestBody:  &NRequest{Account: "abc", Password: "123"},
			useForm:      false,
			expectStatus: http.StatusOK,
			verifyToken:  true,
		},
		{
			requestBody:  &NRequest{Account: "abc", Password: "1234"},
			useForm:      false,
			expectStatus: http.StatusUnauthorized,
			verifyToken:  false,
		},
		{
			requestBody:  &NRequest{Account: "abc", Password: "123"},
			useForm:      true,
			expectStatus: http.StatusOK,
			verifyToken:  true,
		},
		{
			requestBody:  &NRequest{Account: "abc", Password: "1234"},
			useForm:      true,
			expectStatus: http.StatusUnauthorized,
			verifyToken:  false,
		},
	}

	for _, ts := range tests {
		w := httptest.NewRecorder()
		r := buildNRequest(&ts)
		router.ServeHTTP(w, r)
		assert.Equal(t, ts.expectStatus, w.Code)
		verifyToken(t, ts.verifyToken, w, authStrategy)
	}
}

func buildNRequest(ts *NTestSuite) *http.Request {
	var bodyBuffer io.Reader
	var contentType string
	if ts.useForm {
		requestStr := fmt.Sprintf("%s=%s&%s=%s", "account", ts.requestBody.Account, "password", ts.requestBody.Password)
		bodyBuffer = strings.NewReader(requestStr)
		contentType = "application/x-www-form-urlencoded; param=value"
	} else {
		requestBodyBytes, _ := json.Marshal(ts.requestBody)
		bodyBuffer = bytes.NewBuffer(requestBodyBytes)
		contentType = "application/json"
	}
	req, _ := http.NewRequest(http.MethodPost, "/login", bodyBuffer)
	req.Header.Set("Content-Type", contentType)
	return req
}

type NRefreshTestSuite struct {
	requestBody  *NRefreshRequest
	useForm      bool
	expectStatus int
	verifyToken  bool
}

func buildNRefreshRequest(ts *NRefreshTestSuite) *http.Request {
	var bodyBuffer io.Reader
	var contentType string
	if ts.useForm {
		requestStr := fmt.Sprintf("%s=%s", "refresh_token", ts.requestBody.RefreshToken)
		bodyBuffer = strings.NewReader(requestStr)
		contentType = "application/x-www-form-urlencoded; param=value"
	} else {
		requestBodyBytes, _ := json.Marshal(ts.requestBody)
		bodyBuffer = bytes.NewBuffer(requestBodyBytes)
		contentType = "application/json"
	}
	req, _ := http.NewRequest(http.MethodPost, "/refresh", bodyBuffer)
	req.Header.Set("Content-Type", contentType)
	return req
}

func TestRefreshTokenEndpoint(t *testing.T) {
	router, authStrategy := setup()
	encoder := newTokenEncoder(context.Background(), authStrategy, tokenEncoderOptions{maxKeyAge: 10 * time.Minute})
	validRefreshToken, _ := encoder.encode(&RefreshTokenClaims{
		UserId: 1, Account: "abc", ExpirableClaims: ExpirableClaims{ExpiresAt: time.Now().Add(10 * time.Hour).Unix()},
	})
	invalidRefreshToken, _ := encoder.encode(&RefreshTokenClaims{
		UserId: 2, Account: "abc", ExpirableClaims: ExpirableClaims{ExpiresAt: time.Now().Add(-10 * time.Second).Unix()},
	})
	tests := []NRefreshTestSuite{
		{
			requestBody:  &NRefreshRequest{RefreshToken: validRefreshToken},
			useForm:      false,
			expectStatus: http.StatusOK,
			verifyToken:  true,
		},
		{
			requestBody:  &NRefreshRequest{RefreshToken: invalidRefreshToken},
			useForm:      false,
			expectStatus: http.StatusUnauthorized,
			verifyToken:  false,
		},
		{
			requestBody:  &NRefreshRequest{RefreshToken: validRefreshToken},
			useForm:      true,
			expectStatus: http.StatusOK,
			verifyToken:  true,
		},
	}

	for _, ts := range tests {
		w := httptest.NewRecorder()
		r := buildNRefreshRequest(&ts)
		router.ServeHTTP(w, r)
		assert.Equal(t, ts.expectStatus, w.Code)
		verifyToken(t, ts.verifyToken, w, authStrategy)
	}
}

func verifyToken(t *testing.T, verifyToken bool, w *httptest.ResponseRecorder, keyStore PublicKeyStore) {
	if verifyToken {
		response := NResponse{}
		decoder := newTokenDecoder(keyStore)
		bodyBytes, err := io.ReadAll(w.Body)
		logger.Infof("%s", string(bodyBytes))
		if err != nil {
			t.Fatal("fail to read response body")
		}
		err = json.Unmarshal(bodyBytes, &response)
		if err != nil {
			t.Fatal("fail to parse response body")
		}
		at := AccessTokenClaims{}
		err = decoder.Decode(response.AccessToken, &at)
		if err != nil {
			t.Error("fail to parse access token")
		}
		rt := RefreshTokenClaims{}
		err = decoder.Decode(response.RefreshToken, &rt)
		if err != nil {
			t.Error("fail to parse access token")
		}
	}
}
