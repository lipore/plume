package auth

import (
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"net/http"
	"net/http/httptest"
	"testing"
)

func echoHandler(c *gin.Context) {
	c.String(http.StatusOK, "ok")
}

func authZTestingSetup() *gin.Engine {
	r := gin.Default()

	pathAccessMap := map[string][]Access{
		"/a":   {"1", "8"},
		"/b":   {"2", "7"},
		"/a/b": {"3"},
		"/b/a": {"4"},
		"/a/c": {"5"},
		"/b/c": {"6"},
	}
	r.Use(func(c *gin.Context) { c.Set(AccessesXAuthKey, []Access{"1", "4", "5"}) }, ZMiddleware(pathAccessMap))
	r.GET("/a", echoHandler)
	r.GET("/b", echoHandler)
	r.GET("/a/b", echoHandler)
	r.GET("/b/a", echoHandler)
	r.GET("/a/c", echoHandler)
	r.GET("/b/c", echoHandler)
	r.GET("/c", echoHandler)
	return r
}

type ZMiddlewareTestSuite struct {
	path       string
	permission bool
}

func TestZMiddleware(t *testing.T) {
	testSuites := []ZMiddlewareTestSuite{
		{"/a", true},
		{"/b", false},
		{"/a/b", false},
		{"/b/a", true},
		{"/a/c", true},
		{"/b/c", false},
		{"/c", true},
	}
	r := authZTestingSetup()
	for _, ts := range testSuites {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodGet, ts.path, nil)
		r.ServeHTTP(w, req)
		if ts.permission {
			assert.Equal(t, http.StatusOK, w.Code)
		} else {
			assert.Equal(t, http.StatusForbidden, w.Code)
		}
	}
}
