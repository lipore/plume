package auth

import (
	"context"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"github.com/go-playground/assert/v2"
	"testing"
	"time"
)

func TestExpirableClaims_Valid(t *testing.T) {
	at := AccessTokenClaims{
		ExpirableClaims: ExpirableClaims{
			ExpiresAt: time.Now().Add(-1 * time.Second).Unix(),
		},
	}
	assert.Equal(t, at.Valid() == nil, false)
	at = AccessTokenClaims{
		ExpirableClaims: ExpirableClaims{
			ExpiresAt: time.Now().Add(2 * time.Second).Unix(),
		},
	}
	assert.Equal(t, at.Valid() == nil, true)
}

func TestCreateToken(t *testing.T) {
	privateKeyStr := "-----BEGIN PRIVATE KEY-----\nMHcCAQEEIOYu3Y4k84aOgDTCeCmAvhmQEvuc4QXSMzqB/V1d6rrkoAoGCCqGSM49\nAwEHoUQDQgAEC87++eCcFgUSWRDQB1PBIwA1uQWEveRpwPOayvhJdhH2quR8PCE7\nNEq9P5Gutnd6Ba+1BY5yZpaVNWBXBsfYqw==\n-----END PRIVATE KEY-----\n"
	fmt.Printf("%s", privateKeyStr)

	block, _ := pem.Decode([]byte(privateKeyStr))
	privKey, _ := x509.ParseECPrivateKey(block.Bytes)
	keyStore := newInThreadPublicKeyStore()
	encoder := newTokenEncoder(context.Background(), keyStore, tokenEncoderOptions{})
	encoder.privateKey = privKey
	encoder.currentKid = "a"
	decoder := newTokenDecoder(keyStore)
	keyInfo := newKeyInfo(&privKey.PublicKey, time.Now().Add(1*time.Hour), encoder.currentKid)
	keyStore.SavePublicKey(context.Background(), keyInfo)
	claims := AccessTokenClaims{Account: "account", UserId: 1, Issuer: "issuer", Audience: "",
		ExpirableClaims: ExpirableClaims{ExpiresAt: time.Now().Add(1 * time.Hour).Unix()}}
	token, err := encoder.encode(&claims)
	if err != nil {
		t.Fatal(err.Error())
	}
	claims = AccessTokenClaims{}
	err = decoder.Decode(token, &claims)
	if err != nil {
		t.Fatal(err.Error())
	}
	assert.Equal(t, claims.Account, "account")
	assert.Equal(t, claims.UserId, int64(1))
	assert.Equal(t, claims.Issuer, "issuer")
	assert.Equal(t, claims.Audience, "")
}
