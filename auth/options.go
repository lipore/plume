package auth

import (
	"time"
)

var (
	UserIdXAuthKey   = "auth_user_id"
	AccessesXAuthKey = "accesses_user_id"
)

func init() {
	InitWithOptions()
}

type Options func(c *Config)

func WithMaxAccessTokenAge(maxAge time.Duration) func(c *Config) {
	return func(c *Config) {
		c.maxAccessTokenAge = maxAge
	}
}
func WithMaxRefreshTokenAge(maxAge time.Duration) func(c *Config) {
	return func(c *Config) {
		c.maxRefreshTokenAge = maxAge
	}
}

type Config struct {
	maxAccessTokenAge  time.Duration
	maxRefreshTokenAge time.Duration
}

func NewConfig(options ...Options) *Config {
	c := &Config{
		maxAccessTokenAge:  8 * time.Hour,
		maxRefreshTokenAge: 7 * 24 * time.Hour,
	}
	for _, opt := range options {
		opt(c)
	}
	return c
}

var config Config

func InitWithOptions(options ...Options) {
	config = *NewConfig(options...)
}
