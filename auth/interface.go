package auth

import (
	"context"
	"crypto/ecdsa"
)

type PublicKeyStore interface {
	SavePublicKey(ctx context.Context, info *KeyInfo) error
	LoadPublicKey(ctx context.Context, keyId string) *ecdsa.PublicKey
}

type DataSource interface {
	FetchUser(ctx context.Context, userAccount string) User
}

type Access string
