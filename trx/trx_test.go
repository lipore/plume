package trx_test

import (
	"context"
	"errors"
	"gitee.com/lipore/plume/trx"
	"github.com/stretchr/testify/assert"
	"testing"
)

type TxManager struct {
}

func (t *TxManager) Start(ctx context.Context) (trx.Context, error) {
	return &Context{}, nil
}

type recorder struct {
	commit   bool
	rollback bool
}

type Context struct {
	context.Context
	commitErr bool
	r         *recorder
}

func (c *Context) Commit() error {
	if c.commitErr {
		return errors.New("test")
	}
	c.r.commit = true
	return nil
}

func (c *Context) Rollback() {
	c.r.rollback = true
}

func TestTx(t *testing.T) {
	trx.Setup(&TxManager{})
	r := &recorder{commit: false, rollback: false}
	assert.Nil(t, trx.Tx(context.Background(), func(c trx.Context) error {
		if ctx, ok := c.(*Context); ok {
			ctx.r = r
			return nil
		}
		return errors.New("context not correct")
	}))
	assert.True(t, r.commit)
	assert.Nil(t, trx.Tx(context.Background(), func(c trx.Context) error {
		return trx.Tx(c, func(c2 trx.Context) error {
			if ctx, ok := c2.(*Context); ok {
				ctx.r = r
				return nil
			}
			return errors.New("context not correct")
		})
	}))
	assert.True(t, r.commit)
	r = &recorder{commit: false, rollback: false}
	assert.NotNil(t, trx.Tx(context.Background(), func(c trx.Context) error {
		if ctx, ok := c.(*Context); ok {
			ctx.r = r
			return errors.New("test")
		}
		return errors.New("context not correct")
	}))
	assert.True(t, r.rollback)
	r = &recorder{commit: false, rollback: false}
	assert.NotNil(t, trx.Tx(context.Background(), func(c trx.Context) error {
		if ctx, ok := c.(*Context); ok {
			ctx.r = r
			ctx.commitErr = true
			return nil
		}
		return errors.New("context not correct")
	}))
	assert.True(t, r.rollback)
}

func TestStart(t *testing.T) {
	trx.Setup(&TxManager{})
	r := &recorder{commit: false, rollback: false}

	tx, err := trx.Start(context.Background())
	if err != nil {
		t.Error(err)
	}
	if ctx, ok := tx.(*Context); ok {
		ctx.r = r
	} else {
		t.Error(errors.New("context not correct"))
	}
	err = tx.Commit()
	if err != nil {
		t.Error(err)
	}
	assert.True(t, r.commit)
}
