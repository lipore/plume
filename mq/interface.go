package mq

import "context"

type Message interface {
	Body() []byte
	Ack() error
	NoAck() error
	ID() string
}

type SubscriberRegister interface {
	Subscribe(ctx context.Context, subscription string, onMessage func(message Message) error) error
}

type Publisher interface {
	Publish(ctx context.Context, body []byte) error
}

type PublisherFactory interface {
	NewPublisher(topic string) (Publisher, error)
}
