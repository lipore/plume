package IdGenerator

import (
	"github.com/bwmarrin/snowflake"
)

type IdGenerator interface {
	Next() int64
}

type SnowflakeIdGenerator struct {
	node *snowflake.Node
}

func NewSnowflakeIdGenerator(nid int64) (IdGenerator, error) {
	node, err := snowflake.NewNode(nid)
	if err != nil {
		return nil, err
	}
	return &SnowflakeIdGenerator{
		node: node,
	}, nil
}

func (s *SnowflakeIdGenerator) Next() int64 {
	return s.node.Generate().Int64()
}

var idGenerator IdGenerator

func init() {
	idGen, err := NewSnowflakeIdGenerator(1)
	if err != nil {
		panic(err)
	}
	idGenerator = idGen
}

func Next() int64 {
	return idGenerator.Next()
}
